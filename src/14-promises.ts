function delay(time: number) {
  const promise = new Promise<Boolean>((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
  return promise;
}

(async () => {
  console.log('---'.repeat(10));
  const rta = await delay(3000);
  console.log(rta);
})();
